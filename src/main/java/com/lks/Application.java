package com.lks;

import com.lks.dto.GenderizeDTO;
import com.lks.model.Game;
import com.lks.model.Gender;
import com.lks.model.Player;
import com.lks.model.Tournament;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@SpringBootApplication
public class Application {

    private static final String genderizeURL = "https://api.genderize.io?name=";

    private int inputTournamentScore(BufferedReader br) {
        int tournamentScore = 0;
        try {
            System.out.println("1:100, 2:150, 3:200");
            System.out.println("Select 1 or 2 or 3");
            int option= Integer.parseInt(br.readLine());
            if (option == 1) {
                System.out.println("Tournament will be played for 100 points");
                tournamentScore = 100;
            } else if (option == 2) {
                System.out.println("Tournament will be played for 150 points");
                tournamentScore = 150;
            } else if (option == 3) {
                System.out.println("Tournament will be played for 200 points");
                tournamentScore = 200;
            } else {
                System.out.println("Fuck you! Cant see the integer option? Now start from beginning");
                tournamentScore= 0;
            }
        } catch (Exception e) {
            System.out.println("Bhenchod Madarchod! What did I just say. Enter 1 or 2 or 3");
            tournamentScore = 0;
        }
        return tournamentScore;
    }

    private boolean checkNames(String name) {
        Pattern p = Pattern.compile("[a-zA-Z ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(name);
        boolean b = m.find();

        if (!b) {
            System.out.println("Appa Amma correct agi hesaru kotillva?");
        }
        return b;
    }

    private int inputNumberOfPlayers(BufferedReader bufferedReader){
        int numberOfPlayers = 0;
        try {
            numberOfPlayers= Integer.parseInt(bufferedReader.readLine());
            if (numberOfPlayers < 2) {
                System.out.println("Feeling lonely? Motherfucker! You cant play alone");
                return 0;
            } else if(numberOfPlayers == 2) {
                System.out.println("I feel bad for you folks. But I will allow you to continue");
            } else if(numberOfPlayers > 10) {
                System.out.println("Ley tester. I knew you would do boundary testing first!");
            }
        } catch (Exception e) {
            System.out.println("Mindri, enter an integer between 2 and 10. Dont fuck with me");
            return 0;
        }
        return numberOfPlayers;

    }

    private List<Player> enterPlayerNames(BufferedReader bufferedReader, int numberOfPlayers) {
        List<Player> players = new ArrayList<>();
        try {
            Player player;
            int i=1;
            while (i <= numberOfPlayers){
                System.out.println("Player "+ i);
                String name = bufferedReader.readLine();
                boolean b = checkNames(name);
                if(b) {
                    player = new Player(i, name);
                    Gender gender = getGender(name);
                    player.setGender(gender);
                    players.add(player);
                    i++;

                } else {
                    System.out.println("Re-enter the name");
                }
            }
        }catch (Exception e) {
            System.out.println("Mindri, enter the names properly. Now you start from beginning");
            return null;
        }
        return players;

    }

    private Game enterGameScores(BufferedReader bufferedReader,List<Player> players) {
        Game game;
        try {
            int i = 0;
            game = new Game();
            Map<Player, Integer> playerScoreMap = new HashMap<>();
            boolean isThereAWinner = false;
            while (i< players.size()){
                try {
                    Player player = players.get(i);
                    System.out.println("Enter the score of "+ player.getName());
                    Integer playerScore = Integer.parseInt(bufferedReader.readLine());
                    if (playerScore < 0) {
                        System.out.println("Ley Testers. Yakro e thara kaata idthira? Enter again");
                    } else if (playerScore > 50){
                        System.out.println("Ley Testers. Yakro bari boundary testing madthira? Enter again");
                    } else {
                        if(playerScore == 0) {
                            isThereAWinner = true;
                        }
                        playerScoreMap.put(player, playerScore);
                        i++;
                    }
                } catch (Exception e) {
                    System.out.println("Adyen enter madthiro? Enter again");
                }
            }
            if (isThereAWinner) {
                //Update Player Scores
                for(Player player : players) {
                    player.incrementPlayerScore(playerScoreMap.get(player));
                }
                game.setScoreMap(playerScoreMap);
            } else {
                System.out.println("Ohhooo being smart!");
                System.out.println("There should be one winner in a game atleast with score 0. Renter the data");
                enterGameScores(bufferedReader, players);
            }

        } catch (Exception e) {
            System.out.println("Somewhere you fucked up. Tournament over");
            return null;
        }
        return game;

    }


    private void wrongShowStatistics(BufferedReader br, Game game, List<Player> players) {
        try {
            System.out.println("Was there a wrong show in this game? Enter Y or N");
            String answer = br.readLine();
            if(answer.equals("Y")) {
                System.out.println("Which player made the wrong show? Enter his id value");
                List<Integer> validIdList = new ArrayList<>();
                for (Player player: players) {
                    System.out.println(player.getId() + " : "+ player.getName());
                    validIdList.add(player.getId());
                }
                Integer playerId = Integer.parseInt(br.readLine());
                if(validIdList.contains(playerId)) {
                    game.setWrongShow(true);
                    game.setWrongShowPlayer(players.get(playerId - 1));
                    game.setPenaltyScore(game.getHighestScore());
                } else {
                    throw new Exception("Stop messing with the game");
                }

            }

        } catch (Exception e) {
            System.out.println("I am penalizing you for this chutiyapa! Wont store this information");
        }


    }

    private void calculateGameStatistics(Game game) {
        Map<Player, Integer> scoreMap = game.getScoreMap();
        List<Player> winnerList = new ArrayList<>();
        List<Player> loserList = new ArrayList<>();
        int highestScore = 0;
        for(Map.Entry<Player,Integer> entry: scoreMap.entrySet()) {
            Player player = entry.getKey();
            if(highestScore < entry.getValue()) {
                highestScore = entry.getValue();
            }
            if(entry.getValue() == 0) {
                winnerList.add(player);
            } else {
                loserList.add(player);
            }
        }
        game.setHighestScore(highestScore);
        game.setWinners(winnerList);
        game.setLosers(loserList);
    }

    private void updateTournamentStatistics(Tournament tournament, List<Player> players, int gameCount) {
        List<Player> losersList = updateActivePlayers(players, tournament.getTournamentScore());
        for(Player player : losersList) {
            System.out.println(player.getName() + " lost in this round. HAHAHAHAHA. In your face loser");
        }
        if(losersList != null && losersList.size() > 0) {
            Map<Integer, List<Player>> loserMap = tournament.getLoserMap();
            if(loserMap == null || loserMap.size() == 0) {
                loserMap = new TreeMap<>();
                tournament.setLoserMap(loserMap);
            }
            loserMap.put(gameCount, losersList);
        }

    }

    private List<Player> updateActivePlayers(List<Player> players, int tournamentScore) {
        Iterator iterator = players.iterator();
        List<Player> losers = new ArrayList<>();
        while (iterator.hasNext()) {
            Player player = (Player) iterator.next();
            if(player.getCurrentScore() >= tournamentScore) {
                losers.add(player);
                iterator.remove();
            }
        }
        return losers;
    }

    private void activeGame(BufferedReader br, List<Player> players, Tournament tournament) {
        int gameCount = 1;
        Game game = null;
        List<Game> gameList = new ArrayList<>();
        //Make a copy of players list
        List<Player> activePlayers = new ArrayList<>(players);
        while (activePlayers.size() != 1) {
            System.out.println("Game number "+gameCount);
            System.out.println("Current scores of active players are");
            for(Player player : activePlayers) {
                //Update the game count of each player
                player.setCurrentGameCount(gameCount);
                System.out.println(player.getName() + " = " + player.getCurrentScore());
            }
            System.out.println("Enter the scores of the players in this game");
            game = enterGameScores(br, activePlayers);
            game.setId(gameCount);
/*            System.out.println("Was this game a wrong show?");
            wrongShowStatistics(br, game, activePlayers);*/
            calculateGameStatistics(game);
            updateTournamentStatistics(tournament, activePlayers, gameCount);
            gameList.add(game);
            gameCount++;
            drawLine();
        }
        //set tournament winner
        tournament.setWinner(activePlayers.get(0));
        tournament.setGames(gameList);
    }

    private void printTournamentStatistics(Tournament tournament) {
        int index = 1;
        String title = "PLAYER";

        System.out.println("Tournament statistics");
        for(Map.Entry<Integer, List<Player>> entry: tournament.getLoserMap().entrySet()) {
            List<Player> losers = entry.getValue();
            for(Player player : losers) {
                System.out.println(player.getName() + " is loser number "+ index + ". Lost in game "+ player.getCurrentGameCount());
            }
            index++;
        }
        if(tournament.getWinner().getGender() != null) {
            switch (tournament.getWinner().getGender()) {
                case MALE: title = "KING"; break;
                case FEMALE: title = "QUEEN"; break;
                default: title = "PLAYER"; break;
            }
        }
        System.out.println("The "+title+" of the tournament is "+ tournament.getWinner().getName()+". Final score of the winner: "+ tournament.getWinner().getCurrentScore());
    }

    private void drawLine() {
        System.out.println("-----------------------------------------------------------------------------------------");
    }

    private Gender getGender(String name)
    {
        final String uri = genderizeURL+ name;
        Gender gender = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            GenderizeDTO result = restTemplate.getForObject(uri, GenderizeDTO.class);
            if(result != null && result.getGender() != null) {
                gender = Gender.valueOf(result.getGender().toUpperCase());
                System.out.println("The gender of the player is: "+ gender);
            }

        }catch (Exception e) {
            System.out.println("Error fetching your gender. Looks like you have a unique gender neutral name!");
        }

        return gender;

    }

    public static void main(String[] args) {
        //Start the game
        System.out.println("The FIVE tournament begins!");
        Application application = new Application();
        try {
            Tournament tournament = new Tournament();
            InputStreamReader r=new InputStreamReader(System.in);
            BufferedReader br=new BufferedReader(r);

            //Enter number of players
            System.out.println("Enter number of players: ");
            int numberOfPlayers = application.inputNumberOfPlayers(br);
            if(numberOfPlayers == 0) {
                System.exit(0);
            }


            System.out.println("Enter the names of "+numberOfPlayers+" players");
            List<Player> players = application.enterPlayerNames(br, numberOfPlayers);
            tournament.setPlayers(players);

            System.out.println("Enter the final score: ");
            int tournamentScore = application.inputTournamentScore(br);
            tournament.setTournamentScore(tournamentScore);

            System.out.println("Let the games begin!!");
            application.drawLine();
            application.activeGame(br, players, tournament);

            application.printTournamentStatistics(tournament);

        } catch(Exception e) {
            System.out.println("Exception: "+ e);
        }

    }




}
