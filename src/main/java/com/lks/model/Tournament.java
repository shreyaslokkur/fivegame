package com.lks.model;

import java.util.List;
import java.util.Map;

public class Tournament {

    private int id;
    private Location location;
    private List<Player> players;
    private List<Game> games;
    private int tournamentScore;
    private Player winner;
    private Map<Integer, List<Player>> loserMap;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public int getTournamentScore() {
        return tournamentScore;
    }

    public void setTournamentScore(int tournamentScore) {
        this.tournamentScore = tournamentScore;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public Map<Integer, List<Player>> getLoserMap() {
        return loserMap;
    }

    public void setLoserMap(Map<Integer, List<Player>> loserMap) {
        this.loserMap = loserMap;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "id=" + id +
                ", location=" + location +
                ", players=" + players +
                ", games=" + games +
                ", tournamentScore=" + tournamentScore +
                ", winner=" + winner +
                ", loserMap=" + loserMap +
                '}';
    }
}
