package com.lks.model;

import java.util.List;
import java.util.Map;

public class Game {
    private int id;
    private Map<Player, Integer> scoreMap;
    private List<Player> winners;
    private List<Player> losers;
    private Player wrongShowPlayer;
    private boolean wrongShow;
    private int highestScore;
    private int penaltyScore;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<Player, Integer> getScoreMap() {
        return scoreMap;
    }

    public void setScoreMap(Map<Player, Integer> scoreMap) {
        this.scoreMap = scoreMap;
    }

    public List<Player> getWinners() {
        return winners;
    }

    public void setWinners(List<Player> winners) {
        this.winners = winners;
    }

    public List<Player> getLosers() {
        return losers;
    }

    public void setLosers(List<Player> losers) {
        this.losers = losers;
    }

    public Player getWrongShowPlayer() {
        return wrongShowPlayer;
    }

    public void setWrongShowPlayer(Player wrongShowPlayer) {
        this.wrongShowPlayer = wrongShowPlayer;
    }

    public boolean isWrongShow() {
        return wrongShow;
    }

    public void setWrongShow(boolean wrongShow) {
        this.wrongShow = wrongShow;
    }

    public int getHighestScore() {
        return highestScore;
    }

    public void setHighestScore(int highestScore) {
        this.highestScore = highestScore;
    }


    public int getPenaltyScore() {
        return penaltyScore;
    }

    public void setPenaltyScore(int penaltyScore) {
        this.penaltyScore = penaltyScore;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", scoreMap=" + scoreMap +
                ", winners=" + winners +
                ", losers=" + losers +
                ", wrongShowPlayer=" + wrongShowPlayer +
                ", wrongShow=" + wrongShow +
                ", highestScore=" + highestScore +
                ", penaltyScore=" + penaltyScore +
                '}';
    }
}
