package com.lks.model;

public enum Gender {

    MALE,
    FEMALE,
    TRANSGENDER;

}
