package com.lks.model;

public class Player {

    private int id;
    private String name;
    private int currentScore;
    private int currentGameCount;
    private Gender gender;

    public Player(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getCurrentGameCount() {
        return currentGameCount;
    }

    public void setCurrentGameCount(int currentGameCount) {
        this.currentGameCount = currentGameCount;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    //Depends only on id
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    //Compare only id
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Player other = (Player) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currentScore=" + currentScore +
                ", currentGameCount=" + currentGameCount +
                ", gender=" + gender +
                '}';
    }

    public void incrementPlayerScore(Integer playerScore) {
        this.currentScore = this.currentScore + playerScore;
    }
}
